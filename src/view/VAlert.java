package view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *  @author Piotr Wachulec, 269965
 *  Klasa odpowiadająca za kreowanie i wyświetlanie wyśkakującego okienka z powiadomieniem.
 */
public class VAlert {

    /**
     * Metoda statyczna służąca do wyświetlania i generowania okienka z powiadomieniem.
     * @param title Tytuł okna wyskakującego okienka.
     * @param message Wiadomość, która ma być wyświetlona w oknie.
     */
    public static void display(String title, String message) {
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(250);
        window.setResizable(false);
        window.sizeToScene();

        Label label = new Label();
        label.setText(message);
        label.getStyleClass().addAll("txt");

        Button closeButton = new Button("Close the window");
        closeButton.setPrefHeight(50);
        closeButton.getStyleClass().addAll("btt");
        closeButton.setOnAction(e -> window.close());

        VBox layout = new VBox(10);
        layout.getChildren().addAll(label, closeButton);
        layout.setAlignment(Pos.CENTER);
        layout.setPrefHeight(130);
        layout.getStyleClass().addAll("pane");

        layout.getStylesheets().addAll("view/styles.css");
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.show();
    }
}
