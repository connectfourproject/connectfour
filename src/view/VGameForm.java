package view;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import presenter.IPresenter;

/**
 * @author Piotr Wachulec, 269965
 * Klasa obsługująca główne okno programu, rozszerzająca klasę Application
 */
public class VGameForm extends Application {

    private Stage window;
    private Scene confScene, gameScene;
    private Circle[][] circles;
    private Pane[] panes;
    private IPresenter presenter;

    /**
     * Konstruktor publiczny
     * @param presenter Obiekt klasy implementującej interfejs IPresenter
     */
    public VGameForm(IPresenter presenter) {
        this.presenter = presenter;
    }

    /**
     * Metoda uruchamiająca wątek
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        this.window = stage;
        this.confScene = getConfScene();
        this.gameScene = getGameScene(6, 7);
        this.window.setScene(this.confScene);
        this.window.setTitle("ConnectFour");
        this.window.setResizable(false);
        this.window.sizeToScene();
    }

    /**
     * Metoda pokazujaca formularz konfiguracyjny
     */
    public void showConfForm() {
        this.window.setScene(this.confScene);
        this.window.show();
    }

    /**
     * MEtoda pokazujaca formularz gry
     * @param rows Ilość wierszy na planszy
     * @param cols Ilość kolumn na planszy
     */
    public void showGameScene(int rows, int cols) {
        this.gameScene = this.getGameScene(rows, cols);
        this.window.setScene(this.gameScene);
        this.window.show();
    }

    /**
     * Ustawianie płytki użytkownika na planszy
     * @param row Wiersz planszy
     * @param col Kolumna planszy
     * @param player Wybór użytkownika (gracz, czy komputer)
     */
    public void setField(int row, int col, boolean player) {
        this.circles[col][this.circles[col].length - row - 1].getStyleClass().removeAll();
        this.circles[col][this.circles[col].length - row - 1].getStyleClass().add((player) ? "playerCirc" : "compCirc");
    }

    /**
     * Ustawienie wygrywającej płytki
     * @param row Wiersz planszy
     * @param col Kolumna planszy
     */
    public void setWinningField(int row, int col) {
        this.circles[col][this.circles[col].length - row - 1].getStyleClass().removeAll();
        this.circles[col][this.circles[col].length - row - 1].getStyleClass().add("winCirc");
    }

    /**
     * Metoda produkująca scenę formularza konfiguracyjnego
     * @return Obiekt klasy Scene zawierający formularz konfiguracyjny
     */
    private Scene getConfScene() {

        // Main Pane
        Pane mainPane = new Pane();
        mainPane.setPrefSize(500, 300);

        // Board size Pane
        Pane sizePane = new Pane();
        sizePane.setPrefSize(500, 150);
        sizePane.setLayoutX(0);
        sizePane.setLayoutY(0);
        sizePane.getStyleClass().add("pane");

        Label boardSizeLabel = new Label("Board size:");
        boardSizeLabel.setPrefSize(110, 33);
        boardSizeLabel.setLayoutX(41);
        boardSizeLabel.setLayoutY(59);
        boardSizeLabel.getStyleClass().add("txt");

        Label xLabel = new Label("x");
        xLabel.setLayoutX(325);
        xLabel.setLayoutY(59);
        xLabel.getStyleClass().add("txt");

        Label rowsLabel = new Label("(Rows)");
        rowsLabel.setLayoutX(218);
        rowsLabel.setLayoutY(104);
        rowsLabel.getStyleClass().add("txt");

        Label colsLabel = new Label("(Cols)");
        colsLabel.setLayoutX(383);
        colsLabel.setLayoutY(104);
        colsLabel.getStyleClass().add("txt");

        VNumericTextField rowsField = new VNumericTextField(6);
        rowsField.setPrefWidth(100);
        rowsField.setLayoutX(200);
        rowsField.setLayoutY(50);
        rowsField.getStyleClass().add("inputs");

        VNumericTextField colsField = new VNumericTextField(7);
        colsField.setPrefWidth(100);
        colsField.setLayoutX(360);
        colsField.setLayoutY(50);
        colsField.getStyleClass().add("inputs");

        sizePane.getChildren().addAll(boardSizeLabel, xLabel, rowsLabel, colsLabel, rowsField, colsField);

        // Setting player pane
        Pane playerPane = new Pane();
        playerPane.setPrefSize(500, 150);
        playerPane.setLayoutX(0);
        playerPane.setLayoutY(150);
        playerPane.getStyleClass().add("pane");

        Button playerStartBtt = new Button("You start");
        playerStartBtt.setLayoutX(20);
        playerStartBtt.setLayoutY(10);
        playerStartBtt.setPrefSize(140, 120);
        playerStartBtt.getStyleClass().add("btt");
        playerStartBtt.setOnAction(e -> this.presenter.playerChosen(Integer.parseInt(rowsField.getText()), Integer.parseInt(colsField.getText())));

        Button compStartBtt = new Button("Computer start");
        compStartBtt.setLayoutX(180);
        compStartBtt.setLayoutY(10);
        compStartBtt.setPrefSize(140, 120);
        compStartBtt.getStyleClass().add("btt");
        compStartBtt.setOnAction(e -> this.presenter.compChosen(Integer.parseInt(rowsField.getText()), Integer.parseInt(colsField.getText())));

        Button randomStartBtt = new Button("Random start");
        randomStartBtt.setLayoutX(340);
        randomStartBtt.setLayoutY(10);
        randomStartBtt.setPrefSize(140, 120);
        randomStartBtt.getStyleClass().add("btt");
        randomStartBtt.setOnAction(e -> this.presenter.randomChosen(Integer.parseInt(rowsField.getText()), Integer.parseInt(colsField.getText())));

        playerPane.getChildren().addAll(playerStartBtt, compStartBtt, randomStartBtt);

        mainPane.getChildren().addAll(sizePane, playerPane);

        Scene scene = new Scene(mainPane);
        scene.getStylesheets().addAll("view/styles.css");

        return scene;
    }

    /**
     * Metoda służąca do przygotowania sceny gry.
     * @param rows Ilość wierszy planszy
     * @param cols Ilość kolumn planszy
     * @return Obiekt klasy Scene zawierający formularz z planszą gry
     */
    private Scene getGameScene(int rows, int cols) {

        int fieldSize = 50;
        int width = fieldSize * cols;
        int bttWidth = width - 40;
        int bttHeight = 50;
        int comHeight = bttHeight + 40;
        int height = fieldSize * rows + comHeight;

        // Main Pane
        Pane mainPane = new Pane();
        mainPane.setPrefSize(width, height);

        Pane communicationPane = new Pane();
        communicationPane.setPrefSize(width, comHeight);
        communicationPane.setLayoutX(0);
        communicationPane.setLayoutY(height - comHeight);
        communicationPane.getStyleClass().add("pane");

        Button endBtt = new Button("End game");
        endBtt.setPrefSize(bttWidth, bttHeight);
        endBtt.setLayoutX(width - bttWidth - 20);
        endBtt.setLayoutY(20);
        endBtt.getStyleClass().addAll("btt");
        endBtt.setOnMouseClicked(e -> this.window.close());

        communicationPane.getChildren().addAll(endBtt);

        Pane gamePane = new Pane();
        gamePane.setPrefSize(width, height - comHeight);
        gamePane.setLayoutX(0);
        gamePane.setLayoutY(0);
        gamePane.getStyleClass().add("pane");

        this.circles = new Circle[cols][];
        this.panes = new Pane[cols];
        for (int i = 0; i < cols; i++) {
            this.circles[i] = new Circle[rows];
            this.panes[i] = new Pane();
            this.panes[i].setPrefSize(fieldSize, rows * fieldSize);
            this.panes[i].setLayoutX(i * fieldSize);
            this.panes[i].setLayoutY(0);
            this.panes[i].getStyleClass().add("cols");
            this.panes[i].setId(Integer.toString(i));
            this.panes[i].setOnMouseClicked(e -> this.presenter.colClicked(Integer.parseInt(((Pane) e.getSource()).getId())));

            for (int j = 0; j < rows; j++) {
                this.circles[i][j] = new Circle(0.3 * fieldSize);
                this.circles[i][j].setLayoutX(fieldSize / 2);
                this.circles[i][j].setLayoutY(j * fieldSize + fieldSize / 2);
                this.circles[i][j].getStyleClass().add("emptyCirc");
                this.panes[i].getChildren().add(this.circles[i][j]);
            }

            gamePane.getChildren().add(this.panes[i]);
        }

        mainPane.getChildren().addAll(gamePane, communicationPane);

        Scene scene = new Scene(mainPane);
        scene.getStylesheets().addAll("view/styles.css");
        return scene;
    }

    /**
     *  Metoda służąca do wyłączenia obsługi zdarzenia kliknięcia w panel danej kolumny.
     */
    public void turnOffClickingPane() {
        for (int i = 0; i < panes.length; i++) {
            this.panes[i].setOnMouseClicked(null);
        }
    }
}