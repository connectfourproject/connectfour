package view;

import javafx.scene.control.TextField;

/**
 *  @author Piotr Wachulec, 269965
 *  Klasa rozszerzająca klasę TextField będącą opisem pola tekstowego. Obiekt klasy VNumericTextField jest polem tekstowym przyjmującym tylko liczby typu integer.
 */
public class VNumericTextField extends TextField {

    /**
     * Kontruktor publiczny klasy.
     * @param i Liczba początkowa w polu
     */
    public VNumericTextField(int i) {
        this.setText(Integer.toString(i));
    }

    /**
     *
     * @param i
     * @param i1
     * @param string
     */
    @Override
    public void replaceText(int i, int i1, String string) {
        if (string.matches("[0-9]") || string.isEmpty()) {
            super.replaceText(i, i1, string);
        }
    }

    /**
     *
     * @param string
     */
    @Override
    public void replaceSelection(String string) {
        super.replaceSelection(string);
    }
}
