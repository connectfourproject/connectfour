package model;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MLogic {
    private Area area;
  //  private Position winningPosition; // position that caused win
  //  private Player winner;
    public final static int IN_ROW_TO_WIN = 4;// Connect Four
    public final static int MAX_ROWS = 30;
    public final static int MIN_ROWS = IN_ROW_TO_WIN;
    public final static int DEFAULT_ROWS = 6;
    public final static int DEFAULT_COLS = 7;
    public final static int MAX_COLS = 30;
    public final static int MIN_COLS = IN_ROW_TO_WIN;
    private MoveSolver moveSolver;

    public MLogic() throws InvalidAreaSizeException{
            this(DEFAULT_ROWS, DEFAULT_COLS);
    }

    /**
     * Creates logic instance for area of given size:
     * @param rows means diagonal size
     * @param cols means horizontal size
     * @throws InvalidAreaSizeException if rows or columns in desired range
     */
    public MLogic(int rows, int cols) throws InvalidAreaSizeException {
        if (rows < MIN_ROWS || rows > MAX_ROWS || cols < MIN_COLS || cols > MAX_COLS) {
            throw new InvalidAreaSizeException();
        }
        area = new Area(rows, cols);
        //winner = null;
        //winningPosition = null;
        moveSolver = new MoveSolver(area);
    }

    /**
     * @return random Player value that means who starts the game
     */
    public Player whosFirst() {
        List<Player> players = Collections.unmodifiableList(Arrays.asList(Player.values()));
        return players.get(new Random().nextInt(players.size()));
    }


    /**
     * @return correct position of computer move or
     * throws FullColumnException if there's no place to make a move
     */
    public Position makeComputerMove() throws FullColumnException, FullAreaException {
        int col = moveSolver.makeComputerMove().getCol();
        try {
            //area.isMoveCorrect(col);
            return area.addPosition(col, Player.COMPUTER);
        } catch (InvalidMoveException e){
            System.err.println("Invalid implementation of user move algorithm");
            return null;
        }
    }



    /**
     * Validates and adds user move to Area instance
     * and gets whole Position based on given column
     *
     * @param col is chosen by user column
     * @return valid user move to be displayed
     * @throws InvalidMoveException when move is not valid
     * @throws FullColumnException when move is not valid because of
     * already full column
     * @throws FullAreaException when no move is possible
     */
    public Position addUserMove(int col) throws FullAreaException, InvalidMoveException, FullColumnException {
        //area.isMoveCorrect(col);
        return area.addPosition(col,Player.USER);
    }
    /**
     *
     * @return array of IN_ROW_TO_WIN (often 4) winning positions
     * (or more if winning move caused more than 1 winning combination)
     */
    public Position[] getWinningPositions(Position position) throws PositionNotInAreaException{
        try{
            return area.getWinningPositions(position);
        } catch (PositionNotInAreaException e){
            throw e;
        }
    }


    public Area getArea(){
        return area;
    }

    public boolean isAreaFull(){
        if ( Arrays.stream( area.firstEmptyInCols() ).allMatch( p->p==null ) ){
            return true;
        }
        return false;
    }


}

