package model;

import java.util.Arrays;
import java.util.function.Function;

public class MoveSolver {
    private final static int POINTS_SCALE = 100;
    private final Area area;
    public MoveSolver(Area area){
        this.area = area;
    }

    private int convertScoreToInt(Score score){
        if ( score == null ){
            return 0;
        }
        return (int)(
                Math.log((area.getRows()-score.getCurrentPosition().getRow())+1)
                * Math.pow(score.getInRowIncludingThis(), 5)
                * Math.pow( score.getPotentialCombinations(), 2)
                * Math.pow( score.getPotentiallyWinning(), 2 )
                / MLogic.IN_ROW_TO_WIN * POINTS_SCALE
        );
    }


    private int calculateCombinedScore(Position position) throws PositionNotInAreaException, InvalidIterationException{
        Function[] iteratorsForWinningCombination = {
            (p) -> new Position(((Position)p).getRow()+1, ((Position)p).getCol()),
            (p) -> new Position(((Position)p).getRow(), ((Position)p).getCol()+1),
            (p) -> new Position(((Position)p).getRow()+1, ((Position)p).getCol()+1),
            (p) -> new Position(((Position)p).getRow()-1, ((Position)p).getCol()+1)
        };

        int result = 0;
        for ( Function f : iteratorsForWinningCombination ){
            Score scoreComputer;
            Score scoreUser;
            Score scoreUserIfMovedThatWay = null;
            try{
                scoreComputer = area.calculateScore(position, Player.COMPUTER, f);
                scoreUser = area.calculateScore(position, Player.USER, f);

            } catch( PositionNotInAreaException | InvalidIterationException e){
                throw e;
            }
            Area areaIfMovedThatWay = new Area(area);
            try{
                areaIfMovedThatWay.addPosition(position.getCol(), Player.COMPUTER);
//                System.out.println("SUPPOSEDLY--------------->:\n"+areaIfMovedThatWay.getAreaRepresentation());
                scoreUserIfMovedThatWay = areaIfMovedThatWay.calculateScore(new Position(position.getRow()+1,position.getCol()), Player.USER, f);
            } catch ( FullColumnException| FullAreaException | InvalidMoveException | PositionNotInAreaException e){
                // OK, column may be full, area may be as well, position may be out of array (column full)
            }
            scoreComputer.setInRowIncludingThis( scoreComputer.getInRowIncludingThis() + 0.5 );
            int numericalScoreComputer = convertScoreToInt( scoreComputer );
//            System.out.println("Score for COMPUTER: " + numericalScoreComputer );
            int numericalScoreUser = convertScoreToInt( scoreUser );
 //           System.out.println("Score for USER: " + numericalScoreUser );
  //          System.out.println("Going to subtract: " + convertScoreToInt( scoreUserIfMovedThatWay ));
            int numericalScoreUserAnticipated = convertScoreToInt( scoreUserIfMovedThatWay );

            result += numericalScoreComputer + numericalScoreUser - numericalScoreUserAnticipated;
        }
        if ( position != null ) {
//            System.out.println("For position: " + position.getRow() + ", " + position.getCol() + " combined score is " + result);
        } else {
 //           System.out.println("For null position combined score is " + result);

        }
        return result;
    }

    public Position makeComputerMove() throws FullColumnException, FullAreaException {
        Position[] possibleMoves = Arrays.stream( area.firstEmptyInCols() ).filter(p->p!=null).toArray(Position[]::new);
        if ( possibleMoves.length < 1 ) {
            throw new FullAreaException();
        }

        Position result = possibleMoves[0];
        int resultScore = 0;
        try {
            resultScore = calculateCombinedScore(result);
            for (Position p : possibleMoves) {
//                System.out.println("---------NEW POSITION-------");
//                System.out.println(p.getRow() + ", " + p.getCol());
                int combinedScore = calculateCombinedScore(p);
                if ( p != result && combinedScore > resultScore) {
                    result = p;
                    resultScore = combinedScore;
                }
            }
        } catch ( PositionNotInAreaException | InvalidIterationException e){
            e.printStackTrace();
            System.err.println("Wrong iteration or user moving algorithm");
        }
//        System.out.println("Best move: " + result.getRow() + ", " + result.getCol() + ". Score: " + resultScore);
        return result;
    }


}
