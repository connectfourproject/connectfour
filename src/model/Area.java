package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

public class Area {
    private int rows;
    private int cols;
    private Position[][] positions;

    public Area(int rows, int cols){
        this.rows = rows;
        this.cols = cols;
        positions = new Position[rows][cols];
        for (int r=0; r<rows; r++){
            for (int c=0; c<cols; c++){
                positions[r][c]=new Position(r,c,null);
            }
        }
    }

    public Area(Area area){
        this.rows = area.rows;
        this.cols = area.cols;
        this.positions = new Position[area.positions.length][];
        for ( int r = 0; r < area.positions.length; r++){
           this.positions[r] = new Position[area.positions[r].length];
            for ( int c = 0; c < area.positions[r].length; c++) {
                this.positions[r][c] = new Position(area.positions[r][c]);
            }
        }
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public Position[][] getPositions(){
        return positions;
    }

    private Position getPositionValue(Position position) throws IllegalArgumentException, PositionNotInAreaException{
        if (position == null) {
            throw new IllegalArgumentException();
        }
        int pRow = position.getRow();
        int pCol = position.getCol();
        if ( pRow < 0 || pRow >= rows || pCol < 0 || pCol >= cols){
            throw new PositionNotInAreaException();
        }
        position = positions[position.getRow()][position.getCol()];
        if (position == null ) {
            throw new IllegalArgumentException();
        }
        return position;
    }

    public void setOwner(Position position, Player player) throws PositionNotInAreaException, PositionAlreadyTakenException {
        position = getPositionValue(position);
        position.setOwner(player);
    }


    public Position[] firstEmptyInCols(){
        List<Position> result = new ArrayList<>();
        for ( int c = 0; c < cols; c++ ){
            for ( int r = 0; r < rows; r++ ){
                if ( positions[r][c].getOwner() == null ) {
                    result.add( positions[r][c] );
                    break;
                } if ( r == rows-1 ){
                    result.add(null);
                }
            }
        }
        Position[] resultArr = new Position[result.size()];
        resultArr = result.toArray(resultArr);
        return resultArr;
    }


    public Score calculateScore(Position position, Player player, Function<Position,Position> shifter) throws PositionNotInAreaException, InvalidIterationException {
        if ( position == null ) { //when column is full null may be passed
            return null;
        }

 //       System.out.println("Whose score is calculated? " + player);

        Position fPosition = getPositionValue(position);
        int rowDelta = findDeltas(shifter)[0];
        int colDelta = findDeltas(shifter)[1];

        int neutralOrPlayerElementsInRow = 0;
        int potentiallyWinning = 0;
        int inRow = 0;
        int inRowMax = 0;
        for (int i = 0; i < MLogic.IN_ROW_TO_WIN * 2 - 1; i++) {
//            System.out.println("i: "+ i);
            int row = fPosition.getRow() + rowDelta * (MLogic.IN_ROW_TO_WIN - 1 - i );
            int col = fPosition.getCol() + colDelta * (MLogic.IN_ROW_TO_WIN - 1 - i );
  //          System.out.println(row + ", " + col);
            if (row < 0 || col < 0 || row >= rows || col >= cols) {
                continue;
            }
            Position currentPosition = getPositionValue(new Position(row, col));
  //          System.out.println("Traversing position: " + currentPosition.getRow() +", "+ currentPosition.getCol() + ", owner: "+ currentPosition.getOwner());
            if (currentPosition.getOwner() == player || i == MLogic.IN_ROW_TO_WIN - 1) {
                neutralOrPlayerElementsInRow += 1;
                potentiallyWinning += 1;
                inRow += 1;
            } else if (currentPosition.getOwner() == null) {
                inRowMax = Math.max( inRow, inRowMax );
                inRow = 0;
                neutralOrPlayerElementsInRow += 1;
            } else {
                if (i >= MLogic.IN_ROW_TO_WIN ) {
                    break;
                } else {
                    potentiallyWinning = 0;
                    neutralOrPlayerElementsInRow = 0;
                    inRow = 0;
                    inRowMax = 0;
                }
            }

        }
        inRowMax = Math.max( inRow, inRowMax );
/*        System.out.println("position: " + fPosition.getRow() +
                ", " + fPosition.getCol() + ", potentially winning: " + potentiallyWinning +
                ", neutralOrPlayerElementsInRow: " + neutralOrPlayerElementsInRow +
                ", combinations: " + Math.max(0, (neutralOrPlayerElementsInRow-MLogic.IN_ROW_TO_WIN+1)) +
                ", in row max: " + inRowMax
        );
*/

        return new Score(fPosition, potentiallyWinning, Math.max(0, neutralOrPlayerElementsInRow - MLogic.IN_ROW_TO_WIN + 1), inRowMax );

    }



    public String getAreaRepresentation(){
        final String emptyRepresentation = "-";
        String separator;
        final int maxRepresentationSize = 1;
        StringBuilder sb = new StringBuilder();
        Player owner;
        String ownerRepresentation;
        for ( int r = rows-1; r >= 0; r-- ){
            separator = "";
            for ( int c = 0; c < cols; c++ ){
                owner = positions[r][c].getOwner();
                if ( null == owner ){
                    ownerRepresentation = emptyRepresentation;
                } else {
                    ownerRepresentation = owner.getRepresentation().substring(0,Math.min(owner.getRepresentation().length(), maxRepresentationSize) );
                }
                sb.append( separator + String.format("%"+maxRepresentationSize+"s", ownerRepresentation ) );
                separator = " ";
            }
            sb.append("\n");

        }
        return sb.toString();

    }


    public boolean isFree(Position p) throws PositionNotInAreaException{
        return getPositionValue( p ).isFree();
    }


    public Position addPosition( int col, Player player ) throws FullAreaException, FullColumnException, InvalidMoveException{
        Position[] possibleMoves = Arrays.stream( firstEmptyInCols() ).filter(p->p!=null).toArray(Position[]::new);
        if ( possibleMoves.length < 1 ) {
            throw new FullAreaException();
        }
        if ( isMoveCorrect(col) ) {
            Position currentPosition;
            for (int i = 0; i < rows; i++) {
                try {
                    currentPosition = getPositionValue(new Position(i, col));
                    if ( currentPosition.isFree() ) {
                        try {
                            currentPosition.setOwner(player);
                        } catch (PositionAlreadyTakenException e) {
                            System.err.println("Improper implementation of Area::addPosition method");
                        }
                        return currentPosition;
                    }
                } catch (PositionNotInAreaException e){
                    throw new InvalidMoveException();
                }
            }
            throw new FullColumnException();
        }
        throw new InvalidMoveException();
    }

    public boolean isMoveCorrect(int col) throws FullColumnException, InvalidMoveException {
        if ( col < 0 || col >= cols ){
            throw new InvalidMoveException();
        }
        try{
            if ( !getPositionValue(new Position(rows-1, col)).isFree() ){
                throw new FullColumnException();
            }
        } catch (PositionNotInAreaException e){
            throw new InvalidMoveException();
        }
        return true;
    }




    public Position[] getWinningPositions(Position winningPosition) throws PositionNotInAreaException{
        //return length 4 - (actually MLogic.IN_ROW_TO_WIN) (or more if last move produced multiple winning combinations)
        // max length of adjacent in <position row - (4-1) ; row + (4-1) range
        winningPosition = getPositionValue(winningPosition);
        List<Position> wPositions = new ArrayList<>();

        // iterators to get from one position to another matching
        Function[] iteratorsForWinningCombination = {
                (p) -> (new Position(((Position) p).getRow() + 1, ((Position) p).getCol() )),// same column
                (p) -> (new Position(((Position) p).getRow(), ((Position) p).getCol() + 1)), //same row
                (p) -> (new Position(((Position) p).getRow() + 1, ((Position) p).getCol() + 1)), // diagonally right-upwards
                (p) -> (new Position(((Position) p).getRow() - 1, ((Position) p).getCol() + 1)) // diagonally right-downwards
        };
        try{
            addToWinningCollection(winningPosition, wPositions, iteratorsForWinningCombination );
        } catch (InvalidIterationException e){
            System.err.println("getWinningPosition(): Iteration to get winning combination are wrongly specified");
        }

        Position[] result = new Position[wPositions.size()];
        result = wPositions.toArray(result);
        return result;
    }


    private List<Position> addToWinningCollection(Position winningPosition, List wPositions, Function<Position,Position>... shifters) throws PositionNotInAreaException, InvalidIterationException{
        if (wPositions == null) {
            wPositions = new ArrayList<>();
        }
        winningPosition = getPositionValue(winningPosition);

        int wRow = winningPosition.getRow();
        int wCol = winningPosition.getCol();
        Player winner = winningPosition.getOwner();
        for ( Function<Position,Position> shifter : shifters) {
            int [] deltas = findDeltas( shifter );
            Integer rowDelta = deltas[0];
            Integer colDelta = deltas[1];
            if ( colDelta < 0 || (rowDelta <= 0 && colDelta == 0) ){
                throw new InvalidIterationException();
            }

            // go back to starting (leftmost, but if elements are in the same column: bottommost) position for finding winning combination
            // it's checked if element is within area
            int rowBackCount = IntStream.iterate(MLogic.IN_ROW_TO_WIN-1, i->i-1).limit(MLogic.IN_ROW_TO_WIN)
                    .filter(i->wRow-i*rowDelta>=0 && wRow-i*rowDelta<rows).max().orElse(0);
            int colBackCount = IntStream.iterate(MLogic.IN_ROW_TO_WIN-1, i->i-1).limit(MLogic.IN_ROW_TO_WIN)
                    .filter(i->wCol-i*colDelta>=0 && wCol-i*colDelta<cols).max().orElse(0);
            int backCount = Math.min( rowBackCount, colBackCount );
            int rowPosition = wRow - backCount*rowDelta;
            int colPosition = wCol - backCount*colDelta;
            Position currentPosition = getPositionValue( new Position(rowPosition, colPosition) );


            // iterating from starting position until winning combination is found (e.g. four in a row)
            // or end of area is reached
            // or max. elements needed to consider
            int counter = 0;
            int i = 0;
            while ( i < backCount+MLogic.IN_ROW_TO_WIN ){
                if ( currentPosition.getOwner() == winner) {
                    counter++;
                } else {
                    counter = 0;
                }
                if (counter == MLogic.IN_ROW_TO_WIN) {
                    // move in opposite direction than iteration to find
                    addPositionsEndingAt(wPositions, currentPosition,
                            (position) -> ( new Position(position.getRow()-rowDelta, position.getCol()-colDelta)),
                            MLogic.IN_ROW_TO_WIN);
                    break;
                }
                try {
                    currentPosition = getPositionValue( shifter.apply(currentPosition) );
                } catch (PositionNotInAreaException e){
                    // OK, stepped out of area
                    break;
                }
                i++;
            }
        }
        return wPositions;
    }


    private List<Position> addPositionsEndingAt(List wPositions, Position position, Function<Position,Position> iterate, int howMany ) throws InvalidIterationException, PositionNotInAreaException{
        Position currentPosition;
        try{
            currentPosition = getPositionValue( position );
        } catch ( ArrayIndexOutOfBoundsException e){
            throw new PositionNotInAreaException();
        }
        for ( int i = 0; i < howMany; i++){
            if ( !wPositions.contains( currentPosition ) ) {
                wPositions.add(currentPosition);
            }
            try{
                if ( i != howMany-1 ) {
                    currentPosition = getPositionValue( iterate.apply(currentPosition) );
                }
            } catch (ArrayIndexOutOfBoundsException e){
                throw new InvalidIterationException();
            }
        }

        // wPositions.stream().forEach(p->System.err.print( String.format("%d, %d, %s\n", ((Position)(p)).getRow(),((Position)(p)).getCol(), ((Position)(p)).getOwner().toString()  ) )) ;
        return wPositions;
    }

     private int [] findDeltas(Function<Position,Position> shifter) throws InvalidIterationException{
        int rowDelta = 0, colDelta = 0;
        for ( int i = 0; i < rows; i++){
            for ( int j = 0; j < cols; j++){
                try {
                    Position deltaPosition = new Position(i,j);
                    rowDelta = shifter.apply(deltaPosition).getRow() - deltaPosition.getRow();
                    colDelta = shifter.apply(deltaPosition).getCol() - deltaPosition.getCol();
                    return new int[]{rowDelta, colDelta};
                } catch (ArrayIndexOutOfBoundsException e){
                    // OK
                }
            }
        }
        throw new InvalidIterationException();
    }


}
