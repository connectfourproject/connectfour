package model;

public class Position {
    private int row;
    private int col;
    private Player owner;

    public Position(int row, int col){
        this.row = row;
        this.col = col;
    }
    public Position(int row, int col, Player owner){
        this.row = row;
        this.col = col;
        this.owner = owner;
    }

    public Position(Position p){
        row = p.getRow();
        col = p.getCol();
        owner = p.getOwner();
    }

    public void setOwner(Player owner) throws PositionAlreadyTakenException{
        if ( this.getOwner() != null ){
            throw new PositionAlreadyTakenException();
        }
        this.owner = owner;
    }
    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public Player getOwner() {
        return owner;
    }
   public boolean isFree(){
        return this.getOwner() == null;
    }

    public String toString(){
        return "row: " + row + ", col: " + col + ", owner: " + owner.toString();
    }


}
