package model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AreaTest {
    private final int AREA_ROWS = 6;
    private final static int AREA_COLS = 7;
    private Area area = new Area(AREA_ROWS,AREA_COLS);


    @Before
    public void setUp(){
    /*
    . USER
    * COMPUTER
    - free

    - - - - - - -
    - - - - - - -
    - * - - - - -
    - . - * . - -
    - . - . * - -
    . . . * * * *

    */

        try {
            area.addPosition(1, Player.USER);
            area.addPosition(4, Player.COMPUTER);
            area.addPosition(1, Player.USER);
            area.addPosition(4, Player.COMPUTER);
            area.addPosition(1, Player.USER);
            area.addPosition(1, Player.COMPUTER);

            area.addPosition(0, Player.USER);
            area.addPosition(3, Player.COMPUTER);
            area.addPosition(3, Player.USER);
            area.addPosition(3, Player.COMPUTER);

            area.addPosition(4, Player.USER);
            area.addPosition(5, Player.COMPUTER);
            area.addPosition(2, Player.USER);
            area.addPosition(6, Player.COMPUTER);

        } catch (FullAreaException | FullColumnException |InvalidMoveException e){
            System.err.println("Improper sequence of moves in tests setUp() ");
        }
    }

    @Test
    public void isFree() throws Exception {
        try {
            assertTrue(area.isFree(new Position(1, 0)));
            assertTrue(area.isFree(new Position(4, 1)));
            assertTrue(area.isFree(new Position(1, 2)));
            assertTrue(area.isFree(new Position(3, 3)));
            assertTrue(area.isFree(new Position(3, 4)));
            assertTrue(area.isFree(new Position(1, 5)));
            assertTrue(area.isFree(new Position(1, 6)));

            assertFalse(area.isFree(new Position(0, 0)));
            assertFalse(area.isFree(new Position(3, 1)));
            assertFalse(area.isFree(new Position(0, 2)));
            assertFalse(area.isFree(new Position(2, 3)));
            assertFalse(area.isFree(new Position(2, 4)));
            assertFalse(area.isFree(new Position(0, 5)));
            assertFalse(area.isFree(new Position(0, 6)));
        } catch (PositionNotInAreaException e){
            fail();
        }
    }

    @Test
    public void setOwner() throws Exception {
        try{
            area.setOwner( new Position(1,0), Player.COMPUTER );
            area.setOwner( new Position(4,1), Player.USER );
            assertEquals( Player.COMPUTER, area.getPositions()[1][0].getOwner() );
            assertEquals( Player.USER, area.getPositions()[4][1].getOwner() );
        } catch (PositionNotInAreaException | PositionAlreadyTakenException e){
            fail();
        }
        try{
            area.setOwner( new Position(0,1), Player.COMPUTER );
            fail();
        } catch (PositionAlreadyTakenException e){
            // OK, expected
        } catch (PositionNotInAreaException e){
            fail();
        }
    }


    @Test
    public void isMoveCorrect() throws Exception {
        try{
            assertTrue ( area.isMoveCorrect( 0 ) );
            assertTrue ( area.isMoveCorrect( 1 ) );
            assertTrue ( area.isMoveCorrect( 1 ) );
        } catch ( FullColumnException |InvalidMoveException e ){
            fail();
        }
        try{
            area.addPosition( 1, Player.USER );
            area.addPosition( 1, Player.COMPUTER );
            System.err.println( area.getAreaRepresentation() );
            System.err.println( area.isMoveCorrect( 1 ) );
            fail();
        } catch (FullColumnException e){
            //OK, expected
        } catch (InvalidMoveException e){
            fail();
        } catch ( FullAreaException e){
            fail();
        }
        try{
            area.isMoveCorrect( AREA_COLS );
            fail();
        } catch (FullColumnException e){
            fail();
        } catch (InvalidMoveException e){
            //OK, expected as index equal to AREA_COLS is 1 place out of area
        }
    }
            /*

            - - - - - - -
            - - - - - - -
            - * - - - - -
            - . - * . - -
            - . - . * - -
            . . . * * * *

            */
   /* @Test
    public void areAdjacentInRow() throws Exception {
        try {
            assertFalse(area.areAdjacentInRow(new Position(0, 0)));
            assertFalse(area.areAdjacentInRow(new Position(0, 1)));
            assertFalse(area.areAdjacentInRow(new Position(0, 2)));
            assertTrue(area.areAdjacentInRow(new Position(0, 3)));
            assertTrue(area.areAdjacentInRow(new Position(0, 4)));
            assertTrue(area.areAdjacentInRow(new Position(0, 5)));
            assertTrue(area.areAdjacentInRow(new Position(0, 6)));
            assertFalse(area.areAdjacentInRow(new Position(1, 3)));
            assertFalse(area.areAdjacentInRow(new Position(1, 4)));
        } catch (PositionNotInAreaException e){
            System.err.println("test for areAdjacentInRow(): Position not in area");
            fail();
        }
    }
*/
      /*

            - - - - - - -
            - - - - - - -
            . * - * - - -
            . . - * . - -
            . . * . * - -
            . . . * * * *

            */

  /*  @Test
    public void areAdjacentInColumn() throws Exception {
        try {
            area.addPosition(0, Player.USER);
            area.addPosition(2, Player.COMPUTER);
            area.addPosition(0, Player.USER);
            area.addPosition(3, Player.COMPUTER);
            area.addPosition(0, Player.USER);

        } catch( InvalidMoveException|FullColumnException e){
            System.err.println( "Improper addPosition sequence in test for areDiagonallyAdjacent()" );
            fail();
        }
        try {
            assertFalse(area.areAdjacentInColumn(new Position(0, 1)));
            assertFalse(area.areAdjacentInColumn(new Position(0, 3)));
            assertFalse(area.areAdjacentInColumn(new Position(0, 4)));
            assertFalse(area.areAdjacentInColumn(new Position(0, 5)));
            assertFalse(area.areAdjacentInColumn(new Position(0, 6)));

            System.err.println( area.getAreaRepresentation() );
            assertTrue(area.areAdjacentInColumn(new Position(0, 0)));
            assertTrue(area.areAdjacentInColumn(new Position(1, 0)));
            assertTrue(area.areAdjacentInColumn(new Position(2, 0)));
            assertTrue(area.areAdjacentInColumn(new Position(3, 0)));

            assertFalse(area.areAdjacentInColumn(new Position(4, 0)));
            assertFalse(area.areAdjacentInColumn(new Position(5, 0)));

        } catch (PositionNotInAreaException e){
            System.err.println("test for areAdjacentInColumn(): Position not in area");
            fail();
        }
    }
*/

            /*

            - - - - - - -
            - - - - - - -
            - * * - - - -
            - . . * . - -
            - . * . * . -
            . . . * * * *

            */

   /* @Test
    public void areDiagonallyAdjacent() throws Exception {
        try {
            area.addPosition(5, Player.USER);
            area.addPosition(2, Player.COMPUTER);
            area.addPosition(2, Player.USER);
            area.addPosition(2, Player.COMPUTER);
        } catch( InvalidMoveException|FullColumnException e){
            System.err.println( "Improper addPosition sequence in test for areDiagonallyAdjacent()" );
            fail();
        }
        try{
            assertFalse ( area.areDiagonallyAdjacent( new Position(0,2) ) );
            assertTrue ( area.areDiagonallyAdjacent( new Position(0,5) ) );
            assertTrue ( area.areDiagonallyAdjacent( new Position(1,4) ) );
            assertTrue ( area.areDiagonallyAdjacent( new Position(2,3) ) );
            assertTrue ( area.areDiagonallyAdjacent( new Position(3,2) ) );

        } catch (PositionNotInAreaException e){
            System.err.println("test for areDiagonallyAdjacent(): Position not in area");
            fail();
        }
    }
*/
    @Test
    public void getWinningPositions(){

    }

    @Test
    public void addWinning() throws InvalidIterationException {

    }

    @Test
    public void addPositionsEndingAt() throws InvalidIterationException {

    }
}