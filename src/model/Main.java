package model;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    static MLogic mLogic = null;
    static Player first = Player.USER;
    static Player winner = null;
    static Position[] winningPositions;
    public static void main(String[] args){


        while(true) {
            // defaults: 6,7, check if
            Scanner scanner = new Scanner(System.in);
            System.out.println("Rows (default 6):");
            int rowsEnteredByUser = scanner.nextInt();
            System.out.println("Cols (default 7):");
            int colsEnteredByUser = scanner.nextInt();

            try {
                mLogic = new MLogic(rowsEnteredByUser, colsEnteredByUser);
                break;
            } catch (InvalidAreaSizeException e) {
                System.err.println("Invalid area size!");
            }
        }
        first = mLogic.whosFirst();

            if ( first == Player.COMPUTER) {
                try {
                    makeComputerMoveAndCheckIfWinning();
                } catch (PositionNotInAreaException e) {
                    System.err.println("Wrong computer move implementation!");
                } catch (FullColumnException e) {
                    System.err.println("Area is too small to even move for the first time!");
                } catch (FullAreaException e) {
                System.err.println("Area full! Game over! Shouldn't even move ...");
                }
            }

            while( true ) { // or: while (makeUserMove() mLogic.getWinner() == null ){
                System.out.println(mLogic.getArea().getAreaRepresentation());
                if ( mLogic.isAreaFull() ) {
                    System.err.println("Area full! Game over! No one wins.");
                    break;
                }
                try{
                    if ( makeUserMoveAndCheckIfWinning() ) {
                        winner = Player.USER;
                        Arrays.stream(winningPositions).forEach(wp -> System.err.println(wp.getRow() + ", " + wp.getCol()));
                        System.out.println(mLogic.getArea().getAreaRepresentation());
                        System.out.println("USER WINS!");
                        break;
                    }
                } catch (FullColumnException | PositionNotInAreaException | InvalidMoveException e){
                    System.out.println("Cannot perform than operation. Try again");
                    continue;
                } catch (FullAreaException e) {
                    System.err.println("Area full! Game over! Shouldn't even move ...");
                }
                System.out.println( mLogic.getArea().getAreaRepresentation() );
                if ( mLogic.isAreaFull() ) {
                    System.err.println("Area full! Game over! No one wins.");
                    break;
                }


                try {
                    if (makeComputerMoveAndCheckIfWinning()) {
                        winner = Player.COMPUTER;
                        System.out.println(mLogic.getArea().getAreaRepresentation());
                        System.out.println("COMPUTER WINS!");
                        Arrays.stream(winningPositions).forEach(wp -> System.out.println(wp.getRow() + ", " + wp.getCol())); // winning positions
                        break;
                    }
                } catch (FullColumnException|PositionNotInAreaException e) {
                    System.err.println("Wrong computer move implementation");
                } catch (FullAreaException e) {
                    System.err.println("Area full! Game over! Shouldn't even move ...");
                }
            }
    }


    public static boolean makeUserMoveAndCheckIfWinning() throws FullAreaException, FullColumnException, InvalidMoveException, PositionNotInAreaException{
        Scanner scanner = new Scanner(System.in);
        System.out.println("Which column?");
        Position position;
        int col = scanner.nextInt();

        position = mLogic.addUserMove(col);
        winningPositions = mLogic.getWinningPositions(position);
        if ( winningPositions.length > 0 ){
            return true;
        }
        return false;
    }

    public static boolean makeComputerMoveAndCheckIfWinning() throws FullAreaException, PositionNotInAreaException, FullColumnException {
        Position computerMove;
        computerMove = mLogic.makeComputerMove();
        winningPositions = mLogic.getWinningPositions(computerMove);
        if ( winningPositions.length > 0 ){
            return true;
        }
        return false;
    }
}