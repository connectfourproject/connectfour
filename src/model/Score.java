package model;

public class Score {
    private Position currentPosition;
    private double potentiallyWinning;
    private double potentialCombinations;
    private double inRowIncludingThis;

    public Score(Position p, double winningCount, double combinations, double inRow){
        currentPosition = p;
        potentiallyWinning = winningCount;
        potentialCombinations = combinations;
        inRowIncludingThis = inRow;
    }

    public Position getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(Position currentPosition) {
        this.currentPosition = currentPosition;
    }

    public double getPotentiallyWinning() {
        return potentiallyWinning;
    }

    public void setPotentiallyWinning(double potentiallyWinning) {
        this.potentiallyWinning = potentiallyWinning;
    }

    public double getPotentialCombinations() {
        return potentialCombinations;
    }

    public void setPotentialCombinations(double potentialCombinations) {
        this.potentialCombinations = potentialCombinations;
    }

    public double getInRowIncludingThis(){
        return inRowIncludingThis;
    }
    public void setInRowIncludingThis(double inRow){
        inRowIncludingThis = inRow;
    }


}
