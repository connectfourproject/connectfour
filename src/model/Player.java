package model;

public enum Player {
    USER("."), COMPUTER("*");

    private String representation;
    Player(String representation){
        this.representation = representation;
    }

    public String getRepresentation(){
        return representation;
    }

}
