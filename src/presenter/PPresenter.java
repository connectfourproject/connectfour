package presenter;

import model.*;
import javafx.application.Application;
import javafx.stage.Stage;
import view.VAlert;
import view.VGameForm;
import java.util.Arrays;
import java.util.Random;

/**
 *  @author Piotr Wachulec, 269965
 *  Klasa Presentera w architekturze MVP. Wykorzystuje komponenty zapewnione przez klasy Pakietu View, jednak do poprawnego działania musi implementować interfejs IPresenter.
 */
public class PPresenter extends Application implements IPresenter {

    private MLogic mLogic;
    private Player winner;
    private Position[] winningPositions;
    private VGameForm vgf;
    private Stage stage;

    /**
     *  Konstruktor bezargumentowy klasy PPrezenter (bezargumentowości wymaga metoda Application.launch)
     */
    public PPresenter() {
        this.vgf = new VGameForm(this);
    }

    /**
     * Metoda wykonywana przy uruchomieniu wątku.
     * @param primaryStage Scena główna okna
     * @throws Exception Wyjątki zwracane przez Application
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        this.vgf.start(this.stage);
        this.vgf.showConfForm();
    }

    /**
     * Implementacja metody zadeklarowanej w interfejsie
     * @param rows Ilość wierszy na planszy
     * @param cols Ilość kolumn na planszy
     */
    @Override
    public void compChosen(int rows, int cols) {
        try {
            this.mLogic = new MLogic(rows, cols);
            this.vgf.showGameScene(rows, cols);
            makeComputerMoveAndCheckIfWinning();
        } catch (InvalidAreaSizeException e) {
            VAlert.display("Exception", "Invalid area size! Max 30 x 30");
        } catch (PositionNotInAreaException e) {
            VAlert.display("Exception", "Wrong computer move implementation!");
        } catch (FullColumnException e) {
            VAlert.display("Exception", "Area is too small to even move for the first time!");
        } catch (FullAreaException e) {
            VAlert.display("Exception", "Area full! Game over! Shouldn't even move ...");
        }
    }

    /**
     * Implementacja metody zadeklarowanej w interfejsie
     * @param rows Ilość wierszy na planszy
     * @param cols Ilość kolumn na planszy
     */
    @Override
    public void playerChosen(int rows, int cols) {
        try {
            this.mLogic = new MLogic(rows, cols);
            this.vgf.showGameScene(rows, cols);
        } catch (InvalidAreaSizeException e) {
            VAlert.display("Exception", "Invalid area size!");
        }
    }

    /**
     * Implementacja metody zadeklarowanej w interfejsie
     * @param rows Ilość wierszy na planszy
     * @param cols Ilość kolumn na planszy
     */
    @Override
    public void randomChosen(int rows, int cols) {
        Random rand = new Random();
        if (rand.nextInt() % 2 == 0) {
            compChosen(rows, cols);
        } else {
            playerChosen(rows, cols);
        }
    }

    /**
     * Implementacja metody zdefiniowanej w interfejsie
     * @param i Numer wybranej kolumny
     */
    @Override
    public void colClicked(int i) {
        try {
            if (makeUserMoveAndCheckIfWinning(i)) {
                winner = Player.USER;
                this.vgf.turnOffClickingPane();
                Arrays.stream(winningPositions).forEach(wp -> this.vgf.setWinningField(wp.getRow(), wp.getCol()));
                VAlert.display("End game", "You won!");
            } else {
                if (makeComputerMoveAndCheckIfWinning()) {
                    winner = Player.COMPUTER;
                    this.vgf.turnOffClickingPane();
                    Arrays.stream(winningPositions).forEach(wp -> this.vgf.setWinningField(wp.getRow(), wp.getCol()));
                    VAlert.display("End game", "Computer won!");
                }
            }
        } catch (FullColumnException | PositionNotInAreaException | InvalidMoveException e) {
            VAlert.display("Exception", "Cannot perform than operation. Try again");
        } catch (FullAreaException e) {
            VAlert.display("Exception", "Area full! Game over! Shouldn't even move ...");
        }
        if (mLogic.isAreaFull()) {
            VAlert.display("Exception", "Area full! Game over! No one wins.");
        }
    }

    /**
     * Metoda obsługująca ruch użytkownika i sprawdzenie, czy gra została zakończona z wygraną użytkownika
     * @param col Numer kolumny wybranej przez użytkownika
     * @return Prawdę, jeżeli użytkownik wygrał.
     * @throws FullAreaException Plansza jest pełna i nie można już wykonać ruchu. Remis.
     * @throws FullColumnException Kolumna wybrana przez użytkownika jest już pełna.
     * @throws InvalidMoveException Ruch jest nieprawidłowy.
     * @throws PositionNotInAreaException Wybrana pozycja poza polem gry.
     */
    public boolean makeUserMoveAndCheckIfWinning(int col) throws FullAreaException, FullColumnException, InvalidMoveException, PositionNotInAreaException {
        Position position;
        position = mLogic.addUserMove(col);
        this.vgf.setField(position.getRow(), position.getCol(), true);
        winningPositions = mLogic.getWinningPositions(position);
        if (winningPositions.length > 0) {
            return true;
        }
        return false;
    }

    /**
     * Metoda obsługująca ruch komputera i sprawdzenie, czy gra została zakończona z wygraną komputera
     * @return Prawdę, jeżeli komputer wygrał.
     * @throws FullAreaException Plansza jest pełna i nie można już wykonać ruchu. Remis.
     * @throws FullColumnException Kolumna wybrana przez użytkownika jest już pełna.
     * @throws PositionNotInAreaException Wybrana pozycja poza polem gry.
     */
    public boolean makeComputerMoveAndCheckIfWinning() throws FullAreaException, PositionNotInAreaException, FullColumnException {
        Position computerMove;
        computerMove = mLogic.makeComputerMove();
        this.vgf.setField(computerMove.getRow(), computerMove.getCol(), false);
        winningPositions = mLogic.getWinningPositions(computerMove);
        if (winningPositions.length > 0) {
            return true;
        }
        return false;
    }
}