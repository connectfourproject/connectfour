package presenter;


/**
 * @author Piotr Wachulec, 269965
 * Interfejs, dzięki któremu możliwe jest obsłużenie zdarzeń związanych z klikaniem przycisków i pól formularza.
 */
public interface IPresenter {

    /**
     * Metoda wykonywana po wybraniu komputera jako rozpoczynającego grę.
     * @param rows Ilość wierszy planszy
     * @param cols Ilość kolumn planszy
     */
    void compChosen(int rows, int cols);

    /**
     * Metoda wykonywana po wybraniu gracza jako rozpoczynającego grę.
     * @param rows Ilość wierszy planszy
     * @param cols Ilość kolumn planszy
     */
    void playerChosen(int rows, int cols);

    /**
     * Metoda wykonywana po wybraniu losowo rozpoczynającego grę.
     * @param rows Ilość wierszy planszy
     * @param cols Ilość kolumn planszy
     */
    void randomChosen(int rows, int cols);

    /**
     * Metoda wykonywana po kliknięciu w wybraną kolumnę.
     * @param i Numer wybranej kolumny
     */
    void colClicked(int i);
}
