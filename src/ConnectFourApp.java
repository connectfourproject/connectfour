import javafx.application.Application;
import presenter.PPresenter;

/**
 * Created by waszek on 04.02.2017.
 */
public class ConnectFourApp {
    public static void main(String [] args) {
        try {
            Application.launch(PPresenter.class);
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
}
